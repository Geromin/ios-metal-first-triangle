#include <metal_stdlib>
using namespace metal;
//O vertex shader, funciona igual vertex shader em outros sistemas de 3d
vertex float4 basic_vertex(//todo vertex shader tem que começar com "vertex". O retorno é um float4
                           const device packed_float3* vertex_array[[buffer(0)]],//O primeiro buffer, o (0) tem a minha array de vertices
                           unsigned int vid[[vertex_id]]){//o index desse vertice
    return float4(vertex_array[vid], 1.0);//eu pego o vertex na vertex_array e o retorno. É aqui que eu faria algebra linear com a matriz MVP
}
//O fragment shader
fragment half4 basic_fragment(){//Todo fragment shader tem que começar com "fragment"
    return half4(1.0);//retorna branco
}
                              


