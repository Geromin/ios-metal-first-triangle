///Baseado no tutorial em https://www.raywenderlich.com/7475-metal-tutorial-getting-started
///parei no final da parte 5
import UIKit
import Metal //pra biblioteca de 3d

class ViewController: UIViewController {
    //É a ligação do meu sistema com a GPU.
    var device: MTLDevice!
    //Tudo na tela do iOS é um herdeiro de CALayer. No caso eu uso um herdeiro de
    //CALayer específico pra Metal.
    var metalLayer: CAMetalLayer!
    //O buffer dos vértices, onde a array de floats que define um triângulo é posta
    var vertexBuffer: MTLBuffer!
    //Os estados e funções de um passo de renderização.
    var pipelineState: MTLRenderPipelineState!
    //A fila de comandos pra GPU
    var commandQueue: MTLCommandQueue!
    //Um timer sincronizado com o refresh da tela
    var timer: CADisplayLink!
    //Responsável por criar a layer onde eu desenharei as coisas
    private func createMetalLayer() {
        metalLayer = CAMetalLayer()          // Cria a layer
        metalLayer.device = device           // Diz que a minha layer vai usar o device que eu criei
        metalLayer.pixelFormat = .bgra8Unorm // Seta o formato dos pixels (blue-green-red-alpha, de 8bits por canal, normalizado
        metalLayer.framebufferOnly = true    // Só deve ser falso se precisar usar compute shaders ou precisar usar a textura resultante da renderização
        metalLayer.frame = view.layer.frame  // A layer que eu criei deve ser igual ao frame da minha view
        view.layer.addSublayer(metalLayer)  // Adiciona a layer que eu criei à minha view
    }
    
    fileprivate func createVertexBuffer() {
        //Essa array de floats descreve um vertice
        let vertexData: [Float]=[
            0.0,  1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0
        ]
        let dataSize = vertexData.count * MemoryLayout.size(ofValue: vertexData[0])//O tamanho da array em bytes.
        vertexBuffer = device.makeBuffer(bytes: vertexData, length: dataSize, options: [])//Passa os dados da array de vertices pro buffer
    }
    
    private func createPipelineState() {
        //Criação dos vertex shaders e fragment shaders
        let defaultLibrary = device.makeDefaultLibrary()!
        let fragmentProgram = defaultLibrary.makeFunction(name: "basic_fragment")
        let vertexProgram = defaultLibrary.makeFunction(name: "basic_vertex")
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        //Configura essa pipeline de renderização com o shaders e definição do formato de cor
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        //Cria o pipeline state a partir da descrição de pipeline que eu criei
        pipelineState = try! device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
    }
    
    private func createTimer() {
        //Toda vez que a tela atualizar o método loop invoca o gameLoop
        timer = CADisplayLink(target:self, selector:#selector(gameLoop))
        timer.add(to: RunLoop.main, forMode: .default)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        device = MTLCreateSystemDefaultDevice()
        createMetalLayer()
        createVertexBuffer()
        createPipelineState()
        commandQueue = device.makeCommandQueue()
        createTimer()
    }
    
    func render(){
        //retorna a textura na qual eu devo desenhar se eu quiser que algo apareça na tela.
        guard let drawable = metalLayer?.nextDrawable() else { return }
        //Onde eu determino os atributos da destinação dos pixels gerados pelo passo de renderização.
        let renderPassDescriptor = MTLRenderPassDescriptor()
        renderPassDescriptor.colorAttachments[0].texture = drawable.texture//A textura de destino, no caso a tela
        renderPassDescriptor.colorAttachments[0].loadAction = .clear//Limpar a cor da textura
        //Cor de limpeza
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColor(
            red: 0.0,
            green: 104.0/255.0,
            blue: 55.0/255.0,
            alpha: 1.0)
        //Os comandos que eu quero executar
        let commandBuffer = commandQueue.makeCommandBuffer()!
        //O codificador de comandos
        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
        //Informa o estado da pipeline a ser usado
        renderEncoder.setRenderPipelineState(pipelineState)
        //Bota o meu buffer no indice zero, como esperado no meu shader
        renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
        //informa que é pra desenhar triângulos, começando no elemento zero, percorrendo 3 elementos e somente uma cópia
        renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 3, instanceCount: 1)
        //finaliza a codificação do comando
        renderEncoder.endEncoding()
        
        //Apresenta a nova textura assim que o desenho terminar
        commandBuffer.present(drawable)
        //executa o comando
        commandBuffer.commit()
    }
    
    @objc func gameLoop(){
        autoreleasepool{
            self.render()
        }//no final do autoreleasepool os objetos recebem um comando de release
        //os objetos alocados dentro de render() só serão desalocados aqui.
    }


}

