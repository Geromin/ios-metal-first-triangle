import UIKit
import Metal
import MetalKit

class GameObject{
    //Os estados e funções de um passo de renderização.
    var pipelineState: MTLRenderPipelineState!
    //A fila de comandos pra GPU
    var commandQueue: MTLCommandQueue!
    //Uma mesh
    var mesh:MTKMesh!
    var context: MyMetalContext
    
    var renderType = "triangle"
    
    init(context:MyMetalContext){
        self.context = context
        createMesh(device: context.device)
        createPipelineState(device: context.device)
        commandQueue = context.device.makeCommandQueue()
    }
    
    func setPrimitiveTypeToTriangle(){
        renderType = "triangle"
    }
    func setPrimitiveTypeToPoint(){
        renderType = "point"
    }
    
    private func createPipelineState(device _device:MTLDevice) {
        //Criação dos vertex shaders e fragment shaders
        let defaultLibrary = _device.makeDefaultLibrary()!
        let fragmentProgram = defaultLibrary.makeFunction(name: "fragment_main")
        let vertexProgram = defaultLibrary.makeFunction(name: "vertex_main")
        //Configura essa pipeline de renderização com o shaders e definição do formato de cor
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        pipelineStateDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mesh.vertexDescriptor)
        //Cria o pipeline state a partir da descrição de pipeline que eu criei
        pipelineState = try! _device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
    }
    private func createMesh(device _device:MTLDevice) {
        //Um allocator serve para manter os dados de uma mesh obtida pela mesh api. É nele que ficam os
        //dados dos vertices.
        let allocator = MTKMeshBufferAllocator(device: _device)
        //Cria uma esfera usando o allocator criado acima. Mas a mesh ainda não está usável aqui
        let mdlMesh = MDLMesh(sphereWithExtent: [0.75, 0.75, 0.75], segments: [8, 8], inwardNormals: false, geometryType: .triangles, allocator: allocator)
        //Converte de uma modelMesh pra uma metalMesh, que é usável pela Metal.
        do{
            try mesh = MTKMesh(mesh:mdlMesh, device:_device)
        }catch{
            print(error)
        }
    }
    func render(){
        //retorna a textura na qual eu devo desenhar se eu quiser que algo apareça na tela.
        guard let drawable = context.metalLayer?.nextDrawable() else { return }
        //Onde eu determino os atributos da destinação dos pixels gerados pelo passo de renderização.
        let renderPassDescriptor = MTLRenderPassDescriptor()
        renderPassDescriptor.colorAttachments[0].texture = drawable.texture//A textura de destino, no caso a tela
        renderPassDescriptor.colorAttachments[0].loadAction = .clear//Limpar a cor da textura
        //Cor de limpeza
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColor(
            red: 0.0,
            green: 104.0/255.0,
            blue: 55.0/255.0,
            alpha: 1.0)
        //Os comandos que eu quero executar
        let commandBuffer = commandQueue.makeCommandBuffer()!
        //O codificador de comandos
        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
        //Informa o estado da pipeline a ser usado
        renderEncoder.setRenderPipelineState(pipelineState)
        //Uma mesh pode ter vários vertex buffers. Quando no blender o artista cria vários grupos de materiais
        //cada grupo será uma mesh. Além de poder fazer a mesh com várias meshes da maneira tradicional (ex.: varios
        //cubos que não foram postos como parte de uma unica mesh)
        renderEncoder.setVertexBuffer(mesh.vertexBuffers[0].buffer, offset: 0, index:0)
        //informa que é pra desenhar triângulos, começando no elemento zero, percorrendo 3 elementos e somente uma cópia
        guard let submesh = mesh.submeshes.first else { fatalError() }

        let primitiveType:MTLPrimitiveType = selectPrimitiveType(renderType)
        renderEncoder.drawIndexedPrimitives(type: primitiveType,
                                            indexCount: submesh.indexCount,
                                            indexType: submesh.indexType,
                                            indexBuffer: submesh.indexBuffer.buffer,
                                            indexBufferOffset: 0)
        //finaliza a codificação do comando
        renderEncoder.endEncoding()
        
        //Apresenta a nova textura assim que o desenho terminar
        commandBuffer.present(drawable)
        //executa o comando
        commandBuffer.commit()
    }
    
    private func selectPrimitiveType(_ renderType:String)->MTLPrimitiveType{
        if(renderType == "triangle"){
            return .triangle
        }
        if(renderType == "point"){
            return .point
        }
        return .triangle
    }
}
