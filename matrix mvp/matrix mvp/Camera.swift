import UIKit

class Camera{
    private var position = [Float](repeating:0, count:3)
    private var focus = [Float](repeating:0, count:3)
    private var viewUp = [Float](repeating:0, count:3)
    private var targetView : UIView
    
    init(targetView view:UIView){
        self.targetView = view
        lookAt(Eye: [0,0,0], Focus: [0,0,0], ViewUp: [0,1,0])
    }
    
    func lookAt(Eye eye:[Float], Focus focus:[Float], ViewUp vUp:[Float]){
        self.position = eye
        self.focus = focus
        self.viewUp = vUp
    }
    
    func viewMatrix()->Matrix4{
        return Matrix4.look(atEyeX: position[0], eyeY: position[1], eyeZ: position[2],
                     focusX: focus[0], focusY: focus[1], focusZ: focus[2],
                     vUpX: viewUp[0], vUpY: viewUp[1], vUpZ: viewUp[2])
    }
    
    func projectionMatrix()->Matrix4{
        return Matrix4.makePerspectiveViewAngle(Matrix4.degrees(toRad: 85.0), aspectRatio: Float(targetView.bounds.size.width / targetView.bounds.size.height), nearZ: 0.01, farZ: 100.0)
    }
}
