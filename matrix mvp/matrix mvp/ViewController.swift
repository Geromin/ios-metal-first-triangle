
import UIKit

class ViewController: UIViewController {
    private var metalContext : MyMetalContext!
    private var cubinho : GameNode!
    override func viewDidLoad() {
        super.viewDidLoad()
        createMetalContext()
        cubinho = GameNode(Context: metalContext, ModelName: "models/cubo")
        metalContext.addGameObject(GameObject: cubinho)
    }
    
    private func createMetalContext(){
        metalContext = MyMetalContext(Target: view)
    }
    


}

