
class Transform {
    var positionX: Float = 0.0
    var positionY: Float = 0.0
    var positionZ: Float = 0.0
    
    var rotationX: Float = 0.0
    var rotationY: Float = 0.0
    var rotationZ: Float = 0.0
    var scale: Float     = 1.0  
    
    func matrix()->Matrix4{
        let mat = Matrix4()
        mat.translate(positionX, y: positionY, z: positionZ)
        mat.rotateAroundX(rotationX, y: rotationY, z: rotationZ)
        mat.scale(scale, y:scale, z:scale)
        return mat
    }
}
