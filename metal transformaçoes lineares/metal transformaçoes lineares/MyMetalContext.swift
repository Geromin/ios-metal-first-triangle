import UIKit
import Metal
import MetalKit

//Uma classe pra embalar o processo de obtenção do device e da criação da layer
class MyMetalContext {
    //É a ligação do meu sistema com a GPU.
    var device: MTLDevice!
    //Tudo na tela do iOS é um herdeiro de CALayer. No caso eu uso um herdeiro de
    //CALayer específico pra Metal.
    var metalLayer: CAMetalLayer!
    //Um timer sincronizado com o refresh da tela
    var timer: CADisplayLink!
    
    var camera: Camera!
    
    var gameObjects = [GameNode]()
    
    init(Target target:UIView){
        device = MTLCreateSystemDefaultDevice()
        createMetalLayer(target)
        createTimer()
        camera = Camera(targetView: target)
        
        camera.lookAt(Eye: [-1, 2, -3], Focus: [0,0,0], ViewUp: [0,1,0])
    }
    
    func addGameObject(GameObject _obj:GameNode){
        gameObjects.append(_obj)
    }
    
    private func createTimer() {
        //Toda vez que a tela atualizar o método loop invoca o gameLoop
        timer = CADisplayLink(target:self, selector: #selector(gameLoop))
        timer.add(to: RunLoop.main, forMode: .default)
    }
    //Responsável por criar a layer onde eu desenharei as coisas
    private func createMetalLayer(_ view:UIView) {
        metalLayer = CAMetalLayer()          // Cria a layer
        metalLayer.device = device           // Diz que a minha layer vai usar o device que eu criei
        metalLayer.pixelFormat = .bgra8Unorm // Seta o formato dos pixels (blue-green-red-alpha, de 8bits por canal, normalizado
        metalLayer.framebufferOnly = true    // Só deve ser falso se precisar usar compute shaders ou precisar usar a textura resultante da renderização
        metalLayer.frame = view.layer.frame  // A layer que eu criei deve ser igual ao frame da minha view
        view.layer.addSublayer(metalLayer)  // Adiciona a layer que eu criei à minha view
    }
    @objc func gameLoop(){
        autoreleasepool{
            renderScene()
        }//no final do autoreleasepool os objetos recebem um comando de release
        //os objetos alocados dentro de render() só serão desalocados aqui.
    }
    
    public func renderScene(){
        gameObjects.forEach{(go:GameNode)in
            go.render(camera: camera)
        }
    }
}


