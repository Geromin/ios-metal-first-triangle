#include <metal_stdlib>
#include <metal_matrix>
using namespace metal;

struct Uniforms{
    float4x4 modelMatrix;
    float4x4 projectionMatrix;
    float4x4 viewMatrix;
};
//Define o formato do Vertice que é passado pro vertex shader.
struct VertexIn{
    float4 position[[attribute(0)]];//Espera a posição do vertice no indice 0
};

struct VertexOut {
    float4 position [[position]];
};

vertex VertexOut vertex_main(const VertexIn vertex_in[[stage_in]],
                          const device Uniforms& uniforms[ [buffer(1)]]){
    float4x4 modelMatrix = uniforms.modelMatrix;
    float4x4 projectionMatrix = uniforms.projectionMatrix;
    float4x4 viewMatrix = uniforms.viewMatrix;
    VertexOut result;
    result.position = projectionMatrix *viewMatrix * modelMatrix * vertex_in.position;
    return result;
}

fragment half4 fragment_main(VertexOut fragments [[stage_in]]) {
    return half4(fragments.position.x,fragments.position.y,fragments.position.z,1.0);
}



