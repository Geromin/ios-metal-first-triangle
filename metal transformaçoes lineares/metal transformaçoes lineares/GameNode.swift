import UIKit
import Metal
import MetalKit
/*O GameNode é uma coisa que pode ser posta no mundo 3d. O mundo 3d em si ainda será definido
 então, por enquanto, só tem o node, que é posto direto no MyMetalContext*/
class GameNode{
    //Os estados e funções de um passo de renderização.
    var pipelineState: MTLRenderPipelineState!
    //A fila de comandos pra GPU
    var commandQueue: MTLCommandQueue!
    //Uma mesh
    var mesh:MTKMesh!
    //O contexto que contêm as coisas necessárias pra renderização
    var context: MyMetalContext
    //As transformações lineares
    var transform = Transform()
    //Qual tipo de primitiva vou usar? TODO: Fazer um enum
    var renderType = "triangle"
    private var vertexDescriptor: MTLVertexDescriptor!
    //Construtor: carrega a mesh, inicia a pipeline e a command queue. Na inicialização da pipeline tb vai
    //carregar os shaders.
    init(Context ctx:MyMetalContext, ModelName modelName:String){
        self.context = ctx
        createMesh(device: context.device, ModelName: modelName)
        createPipelineState(device: context.device)
        commandQueue = context.device.makeCommandQueue()
    }
    
    //TODO: fazer uma unica função que receba o enum que será criado
    func setPrimitiveTypeToTriangle(){
        renderType = "triangle"
    }
    func setPrimitiveTypeToPoint(){
        renderType = "point"
    }
    
    private func createPipelineState(device _device:MTLDevice) {
        //Criação dos vertex shaders e fragment shaders
        let defaultLibrary = _device.makeDefaultLibrary()!
        let fragmentProgram = defaultLibrary.makeFunction(name: "fragment_main")
        let vertexProgram = defaultLibrary.makeFunction(name: "vertex_main")
        //Configura essa pipeline de renderização com o shaders e definição do formato de cor
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.vertexFunction = vertexProgram
        pipelineStateDescriptor.fragmentFunction = fragmentProgram
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
        pipelineStateDescriptor.vertexDescriptor = MTKMetalVertexDescriptorFromModelIO(mesh.vertexDescriptor)
        //Cria o pipeline state a partir da descrição de pipeline que eu criei
        pipelineState = try! _device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
    }
    
    private func createMesh(device _device:MTLDevice, ModelName _modelName:String){
        vertexDescriptor = MTLVertexDescriptor()
        vertexDescriptor.attributes[0].format = .float3
        vertexDescriptor.attributes[0].offset = 0
        vertexDescriptor.attributes[0].bufferIndex = 0
        vertexDescriptor.layouts[0].stride = MemoryLayout<float3>.stride
        
        let meshDescriptor = MTKModelIOVertexDescriptorFromMetal(vertexDescriptor)
        (meshDescriptor.attributes[0] as! MDLVertexAttribute).name = MDLVertexAttributePosition
        
//    Cria a partir de um arquivo - ainda tá zuado.
        let allocator = MTKMeshBufferAllocator(device: context.device)
        guard let fileUrl: URL = Bundle.main.url(forResource: _modelName, withExtension: "obj")
            else{
                fatalError("Falha em criar a url pro asset \(_modelName)")
        }
        let asset: MDLAsset = MDLAsset(url: fileUrl, vertexDescriptor: meshDescriptor, bufferAllocator: allocator)
        let mdlMesh = asset.object(at: 0)as! MDLMesh
        do{
            //[0]    (null)    "MTKModelErrorKey" : "vertex buffer in MDLMesh was not created using a MTKMeshBufferAllocator"
            try mesh = MTKMesh(mesh: mdlMesh, device: _device)
        }catch{
            fatalError(error.localizedDescription)
        }
    }
    
    private func createRenderPassDescriptor(_ drawable: CAMetalDrawable)->MTLRenderPassDescriptor{
        let renderPassDescriptor = MTLRenderPassDescriptor()
        renderPassDescriptor.colorAttachments[0].texture = drawable.texture//A textura de destino, no caso a tela
        renderPassDescriptor.colorAttachments[0].loadAction = .clear//Limpar a cor da textura
        renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColor(
            red: 0.0,
            green: 104.0/255.0,
            blue: 55.0/255.0,
            alpha: 1.0)
        return renderPassDescriptor
    }

    func render(camera:Camera){
        //retorna a textura na qual eu devo desenhar se eu quiser que algo apareça na tela.
        guard let drawable = context.metalLayer?.nextDrawable() else { return }
        //Onde eu determino os atributos da destinação dos pixels gerados pelo passo de renderização.
        let renderPassDescriptor = createRenderPassDescriptor(drawable)
        //Os comandos que eu quero executar
        let commandBuffer = commandQueue.makeCommandBuffer()!
        //O codificador de comandos
        let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor)!
        //Informa o estado da pipeline a ser usado
        renderEncoder.setRenderPipelineState(pipelineState)
        //Uma mesh pode ter vários vertex buffers. Quando no blender o artista cria vários grupos de materiais
        //cada grupo será uma mesh. Além de poder fazer a mesh com várias meshes da maneira tradicional (ex.: varios
        //cubos que não foram postos como parte de uma unica mesh)
        renderEncoder.setVertexBuffer(mesh.vertexBuffers[0].buffer, offset: 0, index:0)
        //Lida com as matrizes, que devem ser postas em uniformes
        let nodeModelMatrix = transform.matrix()
        let uniformBuffer = context.device.makeBuffer(length: MemoryLayout<Float>.size * Matrix4.numberOfElements() * 3, options: [])
        let bufferPointer = uniformBuffer!.contents()
        memcpy(bufferPointer, nodeModelMatrix.raw(), MemoryLayout<Float>.size * Matrix4.numberOfElements())//Copia a model
        memcpy(bufferPointer + MemoryLayout<Float>.size * Matrix4.numberOfElements(),
               camera.projectionMatrix().raw(),
               MemoryLayout<Float>.size * Matrix4.numberOfElements())//copia a projection, pra logo depois da view
        memcpy(bufferPointer + MemoryLayout<Float>.size * Matrix4.numberOfElements() * 2,
               camera.viewMatrix().raw(),
               MemoryLayout<Float>.size * Matrix4.numberOfElements())//Copia a view
        renderEncoder.setVertexBuffer(uniformBuffer, offset:0, index:1)

        
        //informa que é pra desenhar triângulos, começando no elemento zero, percorrendo 3 elementos e somente uma cópia
        guard let submesh = mesh.submeshes.first else { fatalError() }
        
        let primitiveType:MTLPrimitiveType = selectPrimitiveType(renderType)
        renderEncoder.drawIndexedPrimitives(type: primitiveType,
                                            indexCount: submesh.indexCount,
                                            indexType: submesh.indexType,
                                            indexBuffer: submesh.indexBuffer.buffer,
                                            indexBufferOffset: 0)
        //finaliza a codificação do comando
        renderEncoder.endEncoding()
        
        //Apresenta a nova textura assim que o desenho terminar
        commandBuffer.present(drawable)
        //executa o comando
        commandBuffer.commit()
    }
    
    private func selectPrimitiveType(_ renderType:String)->MTLPrimitiveType{
        if(renderType == "triangle"){
            return .triangle
        }
        if(renderType == "point"){
            return .point
        }
        return .triangle
    }
}

