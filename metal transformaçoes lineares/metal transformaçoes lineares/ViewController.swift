import UIKit

class ViewController: UIViewController {
    private var context: MyMetalContext!
    private var gameObject: GameNode!

    @IBOutlet weak var worldView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        context = MyMetalContext(Target: worldView)
        gameObject = GameNode(context: context)
        context.addGameObject(GameObject: gameObject)
    }


    @IBAction func xTranslationChanged(_ sender: UIStepper) {
        let currentValue = Float(sender.value)/100.0
        gameObject.transform.positionX = currentValue
    }
    @IBAction func yTranslationChanged(_ sender: UIStepper) {
        let currentValue = Float(sender.value)/100.0
        gameObject.transform.positionY = currentValue
    }
    @IBAction func zTranslationChanged(_ sender: UIStepper) {
        let currentValue = Float(sender.value)/100.0
        gameObject.transform.positionZ = currentValue
    }
}

